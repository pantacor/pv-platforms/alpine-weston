#!/bin/sh

if [ ! -d /run/dbus ]
then
	echo "Shared storage is not mounted"
	exit 1
fi

udevd -d
sleep 10
mdev -s
export XDG_RUNTIME_DIR=/run/dbus
mkdir -p /usr/share/X11/xkb
udevadm control --reload-rules && udevadm trigger
# weston -i 0 --tty=7 --device=/dev/fb0 --backend=fbdev-backend.so &
weston --debug -i 0 --tty=7 --drm-device=card1 --backend=drm-backend.so --current-mode &
